#!/usr/bin/env perl
use strict ;

if ($ARGV[0] eq "-h"){
    print "keep.pl. A script to KEEP remote conections over ssh\n";
    print "usage:  keep.pl & <<enter>>\n";
    exit;
    }
my $elap = time ;
my $cont = 0 ;
$| = 1 ;
while(1) {
    $cont ++ ;
    sleep 40 ;
    print  "\a" ;
    #printf "%5.2f\n", (time - $elap)/60  if $cont % 40 == 0 ;
}#while
exit ;